package org.example.validator;

import java.util.function.Supplier;

public class ValidationStep<T> {
    private final Supplier<T> supplier;
    private final Validator<T> validator;

    public ValidationStep(Supplier<T> supplier, Validator<T> validator) {
        this.supplier = supplier;
        this.validator = validator;
    }

    public Supplier<T> getSupplier() {
        return supplier;
    }

    public Validator<T> getValidator() {
        return validator;
    }
}
