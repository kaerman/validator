package org.example.validator;

import java.util.function.Supplier;

@FunctionalInterface
public interface Validator<T> {
    void validate(Supplier<T> validateSupplier) throws ValidationException;
}
