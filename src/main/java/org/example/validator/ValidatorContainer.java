package org.example.validator;

import java.util.ArrayList;
import java.util.List;

public class ValidatorContainer {
    private final List<ValidationStep<?>> validationSteps;

    public ValidatorContainer() {
        validationSteps = new ArrayList<>();
    }

    public void addValidator(ValidationStep<?> validationStep) {
        validationSteps.add(validationStep);
    }

    public void executeValidation() {
        for (ValidationStep t : validationSteps) {
            t.getValidator().validate(t.getSupplier());
        }
    }
}
