package org.example.message;

public class RequestMessage {

    private Long terminalNum;
    private String userInfo;

    public Long getTerminalNum() {
        return terminalNum;
    }

    public void setTerminalNum(Long terminalNum) {
        this.terminalNum = terminalNum;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }
}
