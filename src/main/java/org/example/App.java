package org.example;

import org.example.message.RequestMessage;
import org.example.validator.ValidationException;
import org.example.validator.ValidationStep;
import org.example.validator.ValidatorContainer;
import org.example.validator.Validator;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Functional validator supplier for String
        Supplier<String> stringSupplier = () -> "My request is valid";
        Validator<String> stringValidator = (request) -> Optional.ofNullable(
                request.get() != null ? true : null).orElseThrow(ValidationException::new);
        stringValidator.validate(stringSupplier);

        //Functional validator supplier for Long
        Supplier<Long> longSupplier = () -> 10L;
        Validator<Long> longValidator = (request) -> Optional.ofNullable(
                request.get() > 0 ? true : null).orElseThrow(ValidationException::new);
        longValidator.validate(longSupplier);

        //Validator container
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setTerminalNum(1000L);
        requestMessage.setUserInfo("Zamazingo");

        Supplier<Long> terminalNumSupplier = requestMessage::getTerminalNum;
        Supplier<String> userInfoSupplier = requestMessage::getUserInfo;

        Validator<Long> requestMessageTerminalValidator = (requestTerminalNum) -> Optional.ofNullable(
                requestTerminalNum.get() > 10 ? true : null).orElseThrow(ValidationException::new);
        Validator<String> requestMessageUserInfoValidator = (requestUserInfo) -> Optional.ofNullable(
                requestUserInfo.get().contains("Zamazingo") ? true : null).orElseThrow(ValidationException::new);

        ValidationStep<Long> terminalValidationStep = new ValidationStep<>(terminalNumSupplier, requestMessageTerminalValidator);
        ValidationStep<String> userInfoPairStep = new ValidationStep<>(userInfoSupplier, requestMessageUserInfoValidator);


        ValidatorContainer requestMessageValidatorContainer = new ValidatorContainer();
        requestMessageValidatorContainer.addValidator(terminalValidationStep);
        requestMessageValidatorContainer.addValidator(userInfoPairStep);
        requestMessageValidatorContainer.executeValidation();
    }
}
